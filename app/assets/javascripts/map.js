google.maps.event.addDomListener(window, 'load', initMap);

var map;
var heatmap;
var latLng = new google.maps.LatLng(54.601305, -4.680591);
var options = {
  center: latLng,
  zoom: 7,
  mapTypeId: google.maps.MapTypeId.ROADMAP
}
var geocoder;

function initMap() {
  map = new google.maps.Map(document.getElementById('map'), options);
  geocoder = new google.maps.Geocoder();
}

var markers = [];
function mark(items) {
  var ltsum = 0;
  var lnsum = 0;
  var points = [];
  var gradient = [
    'rgba(0, 255, 255, 0)',
    'rgba(0, 255, 255, 1)',
    'rgba(0, 191, 255, 1)',
    'rgba(0, 127, 255, 1)',
    'rgba(0, 63, 255, 1)',
    'rgba(0, 0, 255, 1)',
    'rgba(0, 0, 223, 1)',
    'rgba(0, 0, 191, 1)',
    'rgba(0, 0, 159, 1)',
    'rgba(0, 0, 127, 1)',
    'rgba(63, 0, 91, 1)',
    'rgba(127, 0, 63, 1)',
    'rgba(191, 0, 31, 1)',
    'rgba(255, 0, 0, 1)'
  ];
  clear();
  if (heatmap != null) {
    heatmap.setMap(null);
  }
  for (var i = 0; i < items.length; i++) {
    ltsum = ltsum + items[i][0];
    lnsum = lnsum + items[i][1];
    var coor = new google.maps.LatLng(items[i][0], items[i][1]);
    points.push(coor);
    var markOpt = {
      position: coor,
      title: items[i][2],
    };
    var marker = new google.maps.Marker(markOpt);
    dropMark(marker, i);
    markers.push(marker);
  }
  heatmap = new google.maps.visualization.HeatmapLayer({
    data: points,
    map: map,
    radius: 30,
    gradient: gradient
  });
  lat = ltsum / items.length;
  lon = lnsum / items.length;
  var center = new google.maps.LatLng(lat, lon);
  map.panTo(center);
  map.setZoom(10);
}

function clear() {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(null);
  }
}

function codeAddress(address) {
  geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      for (var i = 0; i < results.length; i++) {
        if (results[i].address_components[results[i].address_components.length - 1].short_name === "GB") {
          backupSearch(results[i].geometry.location.lat(), results[i].geometry.location.lng(), status);
        }
      }
    } else {
      noResults("Cannot find this location :(");
    }
  });
}

function dropMark(marker, i) {
  setTimeout(function () {
    marker.setMap(map);
    marker.setAnimation(google.maps.Animation.DROP);
  }, i * 100);
}
  
function bounce(num) {
  if (markers[num].getAnimation() !== null) {
    markers[num].setAnimation(null);
  } else {
    markers[num].setAnimation(google.maps.Animation.BOUNCE);
  }
}
