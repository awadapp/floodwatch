var searched = false;
var viewing = false;
var messages = [];
var open = null;

function noResults(message) {
  if (searched === false) {
    var term = document.getElementById('search').value;
   if (term !== null && term !== '') {
      var resultBox = document.createElement('div');
      searched = true;
      resultBox.setAttribute('id', 'resultbox');
      var hit = document.createElement('div');
      var heading = document.createElement('p');
      hit.setAttribute('class', 'hit');
      hit.setAttribute('style', 'text-align: center')
      heading.appendChild(document.createTextNode(message));
      hit.appendChild(heading);
      resultBox.appendChild(hit);
      document.getElementById('menu').appendChild(resultBox);
      drop();
    }
  } else {
    searched = false;
    var menu = document.getElementById('menu');
    menu.removeChild(document.getElementById('resultbox'));
    drop();
    noResults(message);
  }
}

function search(event) {
  if (event.keyCode === 13) {
    var input = document.getElementById('search').value;
    if (input !== '') {
      viewing = false;
      $.post("/pages/home",
      {
        postcode: input,
      },
      function(data) {
        if (data[0] !== false) {
          messages = data;
          if (messages.length > 0) {
            genResults();
            drawCircle();
          } else {
            noResults("Hooray, there are no floods in this area!");
          }
        } else {
          codeAddress(input);
        }
      });
    } else {
      messages = [];
      noResults();
    }
  }
}

function backupSearch(lat, long) {
  viewing = false;
  $.post("/pages/home",
  {
    lat: lat,
    long: long,
  },
  function(data) {
    if (data[0] !== false) {
      messages = data;
      if (messages.length > 0) {
        genResults();
        drawCircle();
      } else {
        noResults("Hooray, there are no floods in this area!");
      }
    } else {
      noResults("Cannot find this location :(");
    }
  });
}

function genResults() {
  if (searched === false) {
    var term = document.getElementById('search').value;
    if (term !== null && term !== '') {
      var resultBox = document.createElement('div');
      searched = true;
      resultBox.setAttribute('id', 'resultbox');
      var coords = [];
      for (var i = 0; i < messages.length; i++) {
        var interm = [];
        interm.push(messages[i].lat);
        interm.push(messages[i].lon);
        interm.push(messages[i].warning);
        coords.push(interm);
        pushHit(resultBox, i);
      }
      mark(coords);
      document.getElementById('menu').appendChild(resultBox);
      drop();
    }
  } else {
    searched = false;
    var menu = document.getElementById('menu');
    menu.removeChild(document.getElementById('resultbox'));
    drop();
    genResults();
  }
}

function pushHit(box, num) {
  var hit = document.createElement('div');
  var heading = document.createElement('h1');
  hit.setAttribute('id', num.toString());
  hit.setAttribute('class', 'hit');
  hit.setAttribute('onclick', 'viewResult(this)');
  heading.appendChild(document.createTextNode(messages[num].warning));
  hit.appendChild(heading);
  box.appendChild(hit);
}

function viewResult(hit) {
  var num = parseInt(hit.id);
  if (open === num || open === null) {
    if (viewing === false) {
      var message = document.createElement('p');
      var code = "Severity: ".concat(messages[num].severity);
      var type = "Type: ".concat(messages[num].type);
      var temp = messages[num].time;
      var hour = temp.slice(11, 13);
      var minute = temp.slice(14, 16);
      var day = parseInt(temp.slice(8, 10));
      var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
      var monthnum = parseInt(temp.slice(5, 7));
      var month = months[monthnum - 1];
      var year = temp.slice(0, 4);
      var time = "Last updated: " + hour + ':' + minute + ' - ' + day + ' ' + month + ' ' + year;
      var text = messages[num].message;
      message.appendChild(document.createTextNode(code));
      newLine(message);
      message.appendChild(document.createTextNode(type));
      newLine(message);
      message.appendChild(document.createTextNode(time));
      newLine(message);
      newLine(message);
      message.appendChild(document.createTextNode(text));
      hit.appendChild(message);
      drop();
      open = num;
      viewing = true;
      bounce(num);
    } else {
      removeHit(hit, num);
    }
  } else {
    old = document.getElementById(open);
    removeHit(old, open);
    viewResult(hit);
  }
}

function removeHit(hit, num) {
  hit.removeChild(hit.childNodes[1]);
  drop();
  viewing = false;
  open = null;
  bounce(num);
}

function drop() {
  var elem = document.getElementById('menu');
  if (searched === true) {
    var results = document.getElementById('resultbox');
    var setting = parseInt($(results).outerHeight()) + 41;
    $(elem).animate({height: setting.toString().concat("px")}, 300);
  } else {
    $(elem).animate({height: "40px"}, 300);
  }
}

function newLine(cont) {
  var line = document.createElement('br');
  cont.appendChild(line);
}

function drawCircle() {
  var elem = document.getElementById('graph');
  if (elem.childNodes.length > 0) {
    elem.removeChild(elem.childNodes[0]);
    elem.parentElement.removeChild(elem.parentElement.childNodes[2]);
  }
  var circumference = 70 * Math.PI * 2;
  var fraction = messages.length / messages[0].total;
  var portion = circumference * fraction;
  var left = circumference - portion;
  var svgNS = "http://www.w3.org/2000/svg";
  var gen = document.createElementNS(svgNS, 'circle');
  var label = document.createElement('h1');
  gen.setAttributeNS(null, 'cx', '80');
  gen.setAttributeNS(null, 'cy', '80');
  gen.setAttributeNS(null, 'r', '70');
  gen.setAttributeNS(null, 'stroke', '#8470FF');
  gen.setAttributeNS(null, 'stroke-width', '140');
  gen.setAttributeNS(null, 'fill', '#CCFFCC');
  gen.setAttributeNS(null, 'stroke-dasharray', portion.toString().concat(' '.concat(left.toString())));
  label.setAttribute('id', 'graphlabel');
  elem.appendChild(gen);
  label.appendChild(document.createTextNode('Local alerts against UK totals'));
  elem.parentElement.appendChild(label);
}