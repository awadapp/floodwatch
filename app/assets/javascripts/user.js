var openState = false;

function loginPanel() {
  if (openState === false) {
    var panel = document.createElement('div');
    var username = document.createElement('input');
    var password = document.createElement('input');
    var logButton = document.createElement('div');
    var regButton = document.createElement('div');
    var forgot = document.createElement('div');
    panel.setAttribute('id', 'panel');
    username.setAttribute('id', 'username');
    username.setAttribute('type', 'text');
    username.setAttribute('onkeyup', 'next(event, this)');
    password.setAttribute('id', 'password');
    password.setAttribute('type', 'password');
    password.setAttribute('onkeyup', 'keyLogin(event)');
    logButton.setAttribute('class', 'loginbutton');
    logButton.setAttribute('onclick', 'login()');
    regButton.setAttribute('class', 'loginbutton');
    regButton.setAttribute('onclick', 'regPanel()');
    forgot.setAttribute('id', 'forgot');
    forgot.setAttribute('onclick', 'recovery()');
    panel.appendChild(document.createTextNode('Username:'));
    panel.appendChild(username);
    panel.appendChild(document.createTextNode('Password:'));
    panel.appendChild(password);
    regButton.appendChild(document.createTextNode('Register'));
    panel.appendChild(regButton);
    logButton.appendChild(document.createTextNode('Login'));
    panel.appendChild(logButton);
    forgot.appendChild(document.createTextNode('Forgot your password?'));
    panel.appendChild(forgot);
    document.getElementById('title').appendChild(panel);
    openPanel('300px', true);
  } else {
    removePanel();
    buttonText('Login/Register');
  }
}

function openPanel(setting, bool) {
  var box = document.getElementById('title');
  if (openState === false) {
    $(box).animate({height: setting}, 400);
    if (bool === true) {
      buttonText('Cancel');
    }
    openState = true;
  } else {
    $(box).animate({height: setting}, 400);
    openState = false;
  }
}

function removePanel() {
  var panel = document.getElementById('panel');
  panel.parentElement.removeChild(panel);
  openPanel('110px', true);
}

function next(event, elem) {
  if (event.keyCode === 13) {
    var kids = document.getElementById('panel').childNodes;
    $(kids[Array.prototype.indexOf.call(elem.parentNode.childNodes,elem) + 2]).focus();
  }
}

function regPanel() {
  removePanel();
  var panel = document.createElement('div');
  var username = document.createElement('input');
  var email = document.createElement('input');
  var password = document.createElement('input');
  var confirm = document.createElement('input');
  var submit = document.createElement('div');
  panel.setAttribute('id', 'panel');
  username.setAttribute('id', 'username');
  username.setAttribute('type', 'text');
  username.setAttribute('onkeyup', 'next(event, this)');
  email.setAttribute('id', 'email');
  email.setAttribute('type', 'text');
  email.setAttribute('onkeyup', 'next(event, this)');
  password.setAttribute('id', 'password');
  password.setAttribute('type', 'password');
  password.setAttribute('onkeyup', 'next(event, this)');
  confirm.setAttribute('id', 'confirm');
  confirm.setAttribute('type', 'password');
  confirm.setAttribute('onkeyup', 'keyReg(event)');
  submit.setAttribute('class', 'loginbutton');
  submit.setAttribute('onclick', 'register()');
  submit.setAttribute('style', 'margin-left: 110px')
  panel.appendChild(document.createTextNode('Username:'));
  panel.appendChild(username);
  panel.appendChild(document.createTextNode('Email:'));
  panel.appendChild(email);
  panel.appendChild(document.createTextNode('Password:'));
  panel.appendChild(password);
  panel.appendChild(document.createTextNode('Confirm password:'));
  panel.appendChild(confirm);
  submit.appendChild(document.createTextNode('Submit'));
  panel.appendChild(submit);
  document.getElementById('title').appendChild(panel);
  openPanel('360px', true);
}

function buttonText(text) {
  var button = document.getElementById('account');
  button.removeChild(button.firstChild);
  button.appendChild(document.createTextNode(text));
}

function keyLogin(event) {
  if (event.keyCode === 13) {
    login();
  }
}

function login() {
  var username = document.getElementById('username').value;
  var password = document.getElementById('password').value;
  if (username !== '') {
    if (password !== '') {
      $.post('/sessions/create',
      {
        username: username,
        password: password,
      },
      function(data) {
        if (data.login === true) {
          removePanel();
          //success(true);
          document.cookie="username=" + username;
          document.cookie="login=true";
          document.cookie="admin=".concat(data.admin);
          authenticate(data.admin);
        } else {
          loginError(data.error);
        }
      });
    } else {
      loginError('password');
    }
  } else {
    loginError('username');
  }
}

function authenticate() {
  var button = document.getElementById('account');
  if (readCookie('login') == 'true') {
    button.setAttribute('onclick', 'userCont()');
    buttonText(readCookie('username'));
  } else {
    button.setAttribute('onclick', 'loginPanel()');
    buttonText('Login/Register');
  }
}

function loginError(id) {
  var panel = document.getElementById('panel');
  var child = panel.childNodes;
  child[1].style.borderColor = 'lightgrey';
  child[3].style.borderColor = 'lightgrey';
  var wrong = document.getElementById(id);
  wrong.style.borderColor = "red";
  $(panel).animate({left: '15px'}, 90);
  $(panel).animate({left: '-15px'}, 70);
  $(panel).animate({left: '15px'}, 50);
  $(panel).animate({left: '-15px'}, 70);
  $(panel).animate({left: '0px'}, 90);
  if (id === 'password' || id === 'username') {
    child[3].value = null;
  }
  if (id === 'username') {
    child[1].value = null;
  }
}

function keyReg(event) {
  if (event.keyCode === 13) {
    register();
  }
}

function register() {
  var username = document.getElementById('username').value;
  var email = document.getElementById('email').value;
  var password = document.getElementById('password').value;
  var confirm = document.getElementById('confirm').value;
  var atpos = email.indexOf("@");
  var dotpos = email.lastIndexOf(".");
  if (username !== '') {
    if (email !== '' && atpos >= 1 && dotpos >= atpos + 2 && dotpos + 2 < email.length) {
      if (password.length > 7) {
        if (password === confirm) {
          $.post('/users/register',
          {
            username: username,
            email: email,
            password: password,
          },
          function(data) {
            if (data.register === true) {
              removePanel();
              success(false);
              buttonText('Login/Register');
            } else {
              regError(data.error, data.message);
            }
          });
        } else {
          regError('confirm', 'Passwords must match.');
        }
      } else {
        regError('password', 'Password must be at least 8 characters long.');
      }
    } else {
      regError('email', 'Please, enter a valid email...');
    }
  } else {
    regError('username', 'Please, enter a username...');
  }
}

function regError(id, message) {
  var panel = document.getElementById('panel');
  var child = panel.childNodes;
  child[1].style.borderColor = 'lightgrey';
  child[3].style.borderColor = 'lightgrey';
  child[5].style.borderColor = 'lightgrey';
  child[7].style.borderColor = 'lightgrey';
  var wrong = document.getElementById(id);
  wrong.style.borderColor = "red";
  $(panel).animate({left: '15px'}, 90);
  $(panel).animate({left: '-15px'}, 70);
  $(panel).animate({left: '15px'}, 50);
  $(panel).animate({left: '-15px'}, 70);
  $(panel).animate({left: '0px'}, 90);
  if (id === 'password' || id === 'username' || id === 'email' || id === 'confirm') {
    child[5].value = null;
    child[7].value = null;
  }
  if (id === 'confirm') {
    child[7].setAttribute('placeholder', message);
  }
  if (id === 'password') {
    child[5].setAttribute('placeholder', message);
  }
  if (id === 'email') {
    child[3].value = null;
    child[3].setAttribute('placeholder', message);
  }
  if (id === 'username') {
    child[1].value = null;
    child[1].setAttribute('placeholder', message);
  }
}

function success(bool) {
  var panel = document.createElement('div');
  panel.setAttribute('id', 'panel');
  panel.appendChild(document.createTextNode('Success!'));
  if (bool === true) {
    panel.appendChild(document.createElement('br'));
    panel.appendChild(document.createTextNode('Cookie: '));
    panel.appendChild(document.createTextNode(readCookie('username')));
  }
  document.getElementById('title').appendChild(panel);
  openPanel('140px', false);
  setTimeout(function() {
    removePanel();
  }, 2000);
}

function readCookie(info) {
  if (info !== null && info !== '') {
    var chunk = document.cookie.concat('X').slice(document.cookie.search(info) + info.length + 1, document.cookie.length + 1);
    var cookie = chunk.slice(0, chunk.indexOf(';'));
    return cookie;
  }
}

function recovery() {
  console.log('Will make the password recovery feature later');
}

function userCont() {
  if (openState === false) {
    var panel = document.createElement('div');
    var buttons = document.createElement('div');
    var locations = document.createElement('div');
    var save = document.createElement('div');
    var logout = document.createElement('div');
    panel.setAttribute('id', 'panel');
    locations.setAttribute('id', 'locations');
    buttons.setAttribute('id', 'buttons');
    save.setAttribute('class', 'loginbutton');
    save.setAttribute('onclick', 'saveLocation()');
    logout.setAttribute('class', 'loginbutton');
    logout.setAttribute('onclick', 'logout()');
    save.appendChild(document.createTextNode('Save'));
    logout.appendChild(document.createTextNode('Logout'));
    buttons.appendChild(save);
    buttons.appendChild(logout);
    panel.appendChild(buttons);
    panel.appendChild(locations);
    document.getElementById('title').appendChild(panel);
    makeLocations(locations);
    if (readCookie('admin') == 'true') {
      adminPanel();
    }
    openPanel('200px', true);
  } else {
    removePanel();
    buttonText(readCookie('username'));
  }
}


function logout() {
  document.cookie = "login=false";
  authenticate();
  removePanel();
}

function saveLocation() {
  if (searched === true) {
    $.post('/pages/save',
    {
      name: readCookie('username'),
    },
    function(data) {
      makeLocations(document.getElementById('locations'));
    });
  }
}

function makeLocations(panel) {
  for (var i = 0; i < panel.childNodes.length; i++) {
    panel.removeChild(panel.childNodes[i]);
  }
  var box = document.createElement('div');
  box.setAttribute('style', 'width: 100%');
  box.appendChild(document.createTextNode('Saved Locations:'));
  panel.appendChild(box);
  $.post('/pages/loader',
  {
    name: readCookie('username'),
  },
  function(data) {
    for (var i = 0; i < data.length; i++) {
      var button = document.createElement('div');
      button.setAttribute('class', 'loginbutton');
      button.setAttribute('id', data[i].lat.toString().concat('X'.concat(data[i].long.toString())));
      button.setAttribute('onclick', 'loadLocation(this)');
      button.appendChild(document.createTextNode(data[i].place));
      panel.appendChild(button);
    }
      sizeUp();
  });
}

function sizeUp() {
  var elem = document.getElementById('title');
 // if (openState === true) {
    var results = document.getElementById('panel');
    var setting = parseInt($(results).outerHeight()) + 120;
    $(elem).animate({height: setting.toString().concat("px")}, 150);
 // } else {
   // $(elem).animate({height: "40px"}, 300);
  //}
}

function loadLocation(elem) {
  var text = elem.id;
  document.getElementById('search').value = elem.firstChild.textContent;
  var array = text.split('X');
  backupSearch(parseFloat(array[0]), parseFloat(array[1]));
}

function adminPanel() {
  var box = document.createElement('div');
  var text = document.createElement('div');
  box.setAttribute('id', 'admin');
  text.setAttribute('style', 'width: 100%');
  text.appendChild(document.createTextNode('User Admin Status:'));
  box.appendChild(text);
  $.post('/pages/getusers',
  {
    foo: true
  },
  function(data) {
    for (var i = 0; i < data.length; i++) {
      var button = document.createElement('div');
      button.setAttribute('class', 'loginbutton');
      button.setAttribute('id', data[i].name);
      button.setAttribute('onclick', 'changePrivilege(this)');
      button.appendChild(document.createTextNode(data[i].name.concat(': '.concat(data[i].admin))));
      box.appendChild(button);
    }
    document.getElementById('panel').appendChild(box);
    sizeUp();
  });
}

function changePrivilege(elem) {
  $.post('/pages/changepriv',
  {
    name: elem.id
  },
  function(data) {
    if (data.change == true) {
      var box = document.getElementById('admin');
      box.parentElement.removeChild(box);
      adminPanel();
    }
  });
}