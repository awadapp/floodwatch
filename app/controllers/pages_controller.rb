require 'open-uri'
require 'json'

class Floodmap
  def initialize
    getall = JSON.parse(open("http://environment.data.gov.uk/flood-monitoring/id/floods") { |f| f.read })
    @totals = getall['items'].length
  end
  def getCoords(postcode)
    $place = postcode.capitalize
    status = JSON.parse(open("http://api.postcodes.io/postcodes/#{postcode.split(' ').join}/validate") { |f| f.read })
    if status['result'] == true
      coor = JSON.parse(open("http://api.postcodes.io/postcodes/#{postcode.split(' ').join}") { |f| f.read })
      lat = coor['result']['latitude']
      long = coor['result']['longitude']
      genRes(lat, long)
    else
      $result = [false]
    end
  end
  def genRes(lat, long)
    $lat = lat
    $long = long
    radius = 15
    alert = JSON.parse(open("http://environment.data.gov.uk/flood-monitoring/id/floods?lat=#{lat}&long=#{long}&dist=#{radius}") { |f| f.read })
    $result = Array.new
    alert['items'].each do |item|
      ltln = JSON.parse(open(item['@id']) { |f| f.read })
      hit = Hash.new
      hit[:warning] = item['description']
      hit[:severity] = item['severity']
      kind = item['isTidal']
      if kind == true
        hit[:type] = "Tidal"
      else
        hit[:type] = "Fluvial"
      end
      tm = item['timeMessageChanged']
      tr = item['timeRaised']
      ts = item['timeSeverityChanged']
      timem = Time.new(tm[0..3].to_i, tm[5..6].to_i, tm[8..9].to_i, tm[11..12].to_i, tm[14..15].to_i, tm[17..18].to_i)
      timer = Time.new(tr[0..3].to_i, tr[5..6].to_i, tr[8..9].to_i, tr[11..12].to_i, tr[14..15].to_i, tr[17..18].to_i)
      times = Time.new(ts[0..3].to_i, ts[5..6].to_i, ts[8..9].to_i, ts[11..12].to_i, ts[14..15].to_i, ts[17..18].to_i)
      if timem >= timer
        if timem >= times
          hit[:time] = timem
        else
          hit[:time] = times
        end
      else
        if timer >= times
          hit[:time] = timer
        else
          hit[:time] = times
        end
      end
      hit[:message] = item['message']
      hit[:lat] = ltln['items']['floodArea']['lat']
      hit[:lon] = ltln['items']['floodArea']['long']
      hit[:total] = @totals
      $result.push(hit)
    end
  end
  def save(name)
    userrecord = User.where(name: name).to_a
    $output = Hash.new
    location = Location.new(user: userrecord[0][:id], lat: $lat, long: $long, place: $place)
    if location.save
      $output[:save] = true
    else
      $output[:save] = false
    end
  end
  def loader(name)
    userrecord = User.where(name: name).to_a
    $output = Location.where(user: userrecord[0][:id]).to_a
  end
  def pullusers
    $output = []
    users = User.all
    users.each do |person|
      out = Hash.new
      out[:name] = person.name
      out[:admin] = person.admin
      $output.push(out)
    end
  end
  def privilege(name)
    user = User.find_by(name: name)
    $output = Hash.new
    if user.admin != "true"
      user.admin = "true"
    else
      user.admin = "false"
    end
    if user.save
      $output[:change] = true
    else
      $output[:change] = false
    end
  end
end

$floodmap = Floodmap.new


class PagesController < ApplicationController
  
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }
  
  def home
    if params[:postcode] != nil
      $floodmap.getCoords(params[:postcode].to_s)
      render :json => $result
    elsif params[:lat] != nil && params[:long] != nil
      $floodmap.genRes(params[:lat], params[:long])
      render :json => $result
    end
  end
  
  def save
    if params[:name] != nil
      $floodmap.save(params[:name])
    end
    render :json => $output
  end
  
  def loader
    if params[:name] != nil
      $floodmap.loader(params[:name])
    end
    render :json => $output
  end
  
  def getusers
    if params[:foo] != nil
      $floodmap.pullusers
      render :json => $output
    end
  end
  
  def changepriv
    if params[:name] != nil
      $floodmap.privilege(params[:name])
      render :json => $output
    end
  end
  
end