class SessionsController < ApplicationController
  def new
  end
  
  def create
    output = Hash.new
    user  = User.find_by(name: params[:username].downcase) #email should have been saved in lowercase in database.
    if user != nil
      if user && user.authenticate(params[:password])
      # Log the user in and redirect to the user's show page.
        output[:login] = true
        output[:admin] = user.admin
      else
        flash.now[:danger] = 'Invalid email/password combination'
        output[:login] = false
        output[:error] = 'password'
        
     # render 'new'
      end
    else
      output[:login] = false
      output[:error] = 'username'
    end
    render :json => output
  end
  
  def destroy
  end
  
end
