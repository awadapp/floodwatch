class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }

  # GET /users
  # GET /users.json
  def index
    @users = User.order(:name)
    respond_to do |format|
    format.html # index.html.erb
    format.json { render json: @users}
    render :json => @users
  end
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end
  
def register
   
    username = params[:username]
    email = params[:email]
    password = params[:password]
    output = Hash.new
    
    create(username, email, password, output)
   
      
    render :json => output #failure to communicate registration state will produce client errors.
end
  
def login
    username = params[:username]
    password = params[:password]
    output = Hash.new
    ucheck = User.find_by(name: username)
    pcheck = password
    
  if ucheck != nil
   if pcheck != nil
    output[:login] = true
    puts "Welcome #{username}" # To test in the console
  else
    output[:login] = false
    puts "Noooo!"
   end
  end

  if output[:login] = false
        output[:error] = "username"
        output[:message] = "Sorry, this username is not correct"
  
  else
   output[:login] = false
        output[:error] = "password"
        output[:message] = "Incorrect password."
  end
    
    render :json => output #output is essential

end
  
  


  # POST /users
  # POST /users.json
  def create(name, email, password, output)
    @user = User.new(name: name, email: email, password: password, admin: "false")
    check = User.find_by(name: name)
    vcheck = User.find_by(email: email)
    if vcheck == nil
      if check == nil
        if @user.save
          output[:register] = true
        else
          output[:register] = false
        end
      else
        output[:register] = false
        output[:error] = "username"
        output[:message] = "Sorry, this username is taken."
      end
    else
      output[:register] = false
      output[:error] = "email"
      output[:message] = "This email address is already in use."
    end

    
    return output
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    @user = User.find(params[:id])
    
    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to user_url, notice: "User #{@user.name} was successfully updated." }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to user_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  #private
    # Use callbacks to share common setup or constraints between actions.
   # def set_user
      #@user = User.find(params[:id])
   # end

    # Never trust parameters from the scary internet, only allow the white list through.
    #def user_params
     # params.require(:user).permit(:name, :password_digest, :email)
   # end
    
end
