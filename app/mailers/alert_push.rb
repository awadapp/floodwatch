class AlertPush < ApplicationMailer
  default from: 'alerts@floodwatch.org'
  
  def welcome_email(user)
    @user = user
    @url = 'http://example.com/login'
    mail(to: @user.email, subject: 'Welcome to Flood Watch')
  end
  
end