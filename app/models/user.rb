class User < ActiveRecord::Base
  #validates :login, presence: true
  validates :email, presence: true # uniquiness: true
  validates :name, presence: true # uniqueness: true
  has_secure_password
  validates :password, confirmation: true
end
