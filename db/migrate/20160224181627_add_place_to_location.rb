class AddPlaceToLocation < ActiveRecord::Migration
  def change
    add_column :locations, :place, :string
  end
end
